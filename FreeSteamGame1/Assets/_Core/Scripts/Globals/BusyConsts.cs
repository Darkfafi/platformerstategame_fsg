﻿using UnityEngine;
using System.Collections;

public static class BusyConsts
{
	// Layers
	public const int LAYER_MOVEMENT = 1;

	// Actions
	public const string ACTION_IN_AIR = "ActionInAir";
	public const string ACTION_HOR_MOVING = "ActionHorMovement";


}
