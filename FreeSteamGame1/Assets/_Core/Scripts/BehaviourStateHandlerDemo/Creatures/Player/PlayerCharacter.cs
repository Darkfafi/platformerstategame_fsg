﻿using UnityEngine;
using Ramses.StateMachine;
using System;

[RequireComponent(typeof(InputUser))]
public class PlayerCharacter : Creature, IFreazable
{
	public InputUser InputUser { get; private set; }
	protected IStateMachine<PlayerCharacter> stateMachinePlayer;

	protected override void SetCreature()
	{
		base.SetCreature();
		InputUser = gameObject.GetComponent<InputUser>();
		((CreatureAnimationManager)AnimationManager).SetAnimationHandler(this);
		stateMachinePlayer = new StateMachine<PlayerCharacter, PlayerMovementState>(this);
		stateMachinePlayer.AddAnyStateSwitcher<DebugFrozenSwitcher<PlayerCharacter>>();
	}

	protected void Update()
	{
		if (stateMachinePlayer != null)
		{
			stateMachinePlayer.Update();
		}
	}

	public bool Freeze(StateInfo info)
	{
		return stateMachinePlayer.SwitchToState<FrozenState<PlayerCharacter>>(info);
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		stateMachinePlayer.DestroyStateMachine();
	}
}
