﻿using UnityEngine;
using System.Collections;

public class CreatureAnimationManager : AnimationManager {

	protected Creature creature { private set; get; }

	public void SetAnimationHandler(Creature creature)
	{
		this.creature = creature;
	}

	public override string GetAnimationName(string animationString)
	{
		return base.GetAnimationName(animationString);
	}
}
