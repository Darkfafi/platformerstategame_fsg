﻿using UnityEngine;
using System;

[Serializable]
public struct MovementStats
{
	public float Speed;
	public float JumpHeight;
	public int DubbleJumps;
}
