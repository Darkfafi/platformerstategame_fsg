﻿using UnityEngine;
using Ramses.StateMachine;

public class FrozenState<T> : State<T> where T : Creature
{
	private Color originalColor;
	private float timeFrozen = 0f;
	private float duration;

	protected override void StateStart(StateInfo info)
	{
		originalColor = effected.SpriteRenderer.color;
        effected.AnimationManager.PauseCurrentAnimation();
		effected.SpriteRenderer.color = Color.cyan;
		if (!stateMachine.WasInState<FrozenState<T>>())
		{
			duration = 0f;
			timeFrozen = 0f;
		}
		if (info.Get<StateDurationInfo>() != null)
		{
			duration += info.Get<StateDurationInfo>().Duration - timeFrozen;
		}
		else
		{
			duration += 2f - timeFrozen;
		}
    }

	protected override void StateUpdate()
	{
		timeFrozen += Time.deltaTime;
		if(timeFrozen >= duration)
		{
			stateMachine.SwitchToPreviousState();
		}
    }

	protected override void StateEnd()
	{
        effected.SpriteRenderer.color = originalColor;
	}
}
