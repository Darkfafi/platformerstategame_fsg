﻿using UnityEngine;
using Ramses.StateMachine;

public interface IFreazable
{
	bool Freeze(StateInfo info); // Boolean returns success of freeze.
}
