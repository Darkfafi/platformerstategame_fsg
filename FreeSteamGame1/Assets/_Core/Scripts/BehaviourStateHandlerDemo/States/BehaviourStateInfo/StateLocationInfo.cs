﻿using UnityEngine;
using Ramses.StateMachine;

public class StateLocationInfo :  StateInfoParameter
{
	public Vector2 Location;

	public StateLocationInfo(Vector2 location)
	{
		Location = location;
    }
}
