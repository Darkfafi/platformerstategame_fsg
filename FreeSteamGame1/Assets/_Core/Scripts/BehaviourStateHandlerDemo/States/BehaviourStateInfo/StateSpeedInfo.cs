﻿using Ramses.StateMachine;

public class StateSpeedInfo : StateInfoParameter
{
	public float Speed;

	public StateSpeedInfo(float speed)
	{
		Speed = speed;
	}
}
