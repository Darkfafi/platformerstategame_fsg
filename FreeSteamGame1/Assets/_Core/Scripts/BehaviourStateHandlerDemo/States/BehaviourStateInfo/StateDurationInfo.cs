﻿using Ramses.StateMachine;

public class StateDurationInfo : StateInfoParameter
{
	public float Duration;

	public StateDurationInfo(float durationInSeconds)
	{
		Duration = durationInSeconds;
    }
}
