﻿using Ramses.StateMachine;

public class StateResourceLocationInfo : StateInfoParameter
{
	public string ResourceLocation;

	public StateResourceLocationInfo(string resourceLocation)
	{
		ResourceLocation = resourceLocation;
	}
}
