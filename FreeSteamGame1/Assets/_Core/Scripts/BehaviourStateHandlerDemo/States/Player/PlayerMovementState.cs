﻿using UnityEngine;
using Ramses.StateMachine;

public class PlayerMovementState : State<PlayerCharacter>
{
	private PlayerCharacter player;
	private BusyList busyList  { get { return player.BusyList; } }

	protected override void StateStart(StateInfo info)
	{
		player = effected;
		busyList.RemoveBusyAction(BusyConsts.LAYER_MOVEMENT);

		if (player == null)
		{
			Debug.LogError("State activated on a non Player Character!");
		}

		AddStateSwitcher<PlayerCombatSwitcher>();

		player.PlatformerMovement2D.MovementSpeed = player.MovementStats.Speed;
		player.PlatformerMovement2D.JumpHeight = player.MovementStats.JumpHeight;
		player.PlatformerMovement2D.MaxJumps = player.MovementStats.DubbleJumps + 1;

		RemoveEventListeners();
		AddEventListeners();
	}

	protected override void StateUpdate()
	{
		if(!busyList.InBusyAction())
		{
			player.AnimationManager.PlayAnimation("Idle");
		}

		if (busyList.InBusyAction(BusyConsts.ACTION_IN_AIR))
		{
			if (player.PlatformerMovement2D.Velocity.y > 0.4f)
			{
				player.AnimationManager.PlayAnimation("JumpUp");
			}
			if (player.PlatformerMovement2D.Velocity.y < 0.4f)
			{
				player.AnimationManager.PlayAnimation("JumpDown");
			}
		}
	}

	protected override void StateEnd()
	{
		busyList.RemoveBusyAction(BusyConsts.LAYER_MOVEMENT);
		RemoveEventListeners();
	}

	private void AddEventListeners()
	{
		player.InputUser.InputEvent += InputUsedEvent;
		player.PlatformerMovement2D.MoveEvent += OnMoveEvent;
		player.PlatformerMovement2D.LandOnGroundEvent += OnLandOnGroundEvent;
		player.PlatformerMovement2D.LostContactWithGroundEvent += OnLostContactWithGroundEvent;
	}

	private void RemoveEventListeners()
	{
		player.InputUser.InputEvent -= InputUsedEvent;
		player.PlatformerMovement2D.MoveEvent -= OnMoveEvent;
		player.PlatformerMovement2D.LandOnGroundEvent -= OnLandOnGroundEvent;
		player.PlatformerMovement2D.LostContactWithGroundEvent -= OnLostContactWithGroundEvent;
	}

	private void InputUsedEvent(InputAction action, InputUser user)
	{
		if (action.KeyActionValue == InputAction.KeyAction.KeyDown)
		{
			if (action.Name == InputNames.LEFT)
			{
				player.PlatformerMovement2D.Move(PlatformerMovement2D.DIR_LEFT);
			}
			else if (action.Name == InputNames.RIGHT)
			{
				player.PlatformerMovement2D.Move(PlatformerMovement2D.DIR_RIGHT);
			}
		}
		else if (action.KeyActionValue == InputAction.KeyAction.OnKeyDown)
		{
			if(action.Name == InputNames.JUMP)
				player.PlatformerMovement2D.Jump();
		}
		else if(action.KeyActionValue == InputAction.KeyAction.OnKeyUp)
		{
			if (action.Name == InputNames.LEFT || action.Name == InputNames.RIGHT)
			{
				player.PlatformerMovement2D.StopMoveVelocity();
				busyList.RemoveBusyAction(BusyConsts.ACTION_HOR_MOVING, BusyConsts.LAYER_MOVEMENT);
			}
		}
	}

	private void OnLandOnGroundEvent()
	{
		busyList.RemoveBusyAction(BusyConsts.ACTION_IN_AIR, BusyConsts.LAYER_MOVEMENT);
	}

	private void OnLostContactWithGroundEvent()
	{
		busyList.AddBusyAction(BusyConsts.ACTION_IN_AIR, BusyConsts.LAYER_MOVEMENT);
	}

	private void OnMoveEvent(float velocity)
	{
		busyList.AddBusyAction(BusyConsts.ACTION_HOR_MOVING, BusyConsts.LAYER_MOVEMENT);
		if(!busyList.InBusyAction(BusyConsts.ACTION_IN_AIR))
		{
			player.AnimationManager.PlayAnimation("Run");
		}
	}
}
