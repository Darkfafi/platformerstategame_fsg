﻿using UnityEngine;
using System.Collections;
using Ramses.StateMachine;

public abstract class BaseCreatureAttackState<T> : State<T> where T : Creature
{
	protected float cooldownInSeconds = 0.1f;
	private float lastTimeUsed = 0;

	public override bool IsAbleToActivate(T user)
	{
		if (Time.time - lastTimeUsed > cooldownInSeconds)
		{
			return true;
		}
		return false;
	}

	protected override void StateEnd()
	{
		lastTimeUsed = Time.time;
	}
}