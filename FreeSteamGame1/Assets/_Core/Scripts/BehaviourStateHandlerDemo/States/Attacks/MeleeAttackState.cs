﻿using UnityEngine;
using Ramses.StateMachine;
using System;

public class MeleeAttackState<T> : BaseCreatureAttackState<T> where T : Creature
{
	protected override void StateStart(StateInfo info)
	{

		effected.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		effected.AnimationManager.AnimationEndedEvent += OnAnimationEndedEvent;

		if(effected.PlatformerMovement2D.OnGround)
		{
			effected.PlatformerMovement2D.StopMoveVelocity();
		}

		effected.AnimationManager.PlayAnimation("Attack");
	}

	protected override void StateEnd()
	{
		base.StateEnd();
		effected.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
	}

	private void OnAnimationEndedEvent(string animation, float finishNormTime)
	{
		if (effected.AnimationManager.CurrentAnimationNameCheck("Attack"))
		{
			stateMachine.SwitchToDefaultState();
		}
	}

	protected override void StateUpdate()
	{

	}
}
