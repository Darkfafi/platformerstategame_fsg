﻿using System;
using Ramses.StateMachine;
using UnityEngine;

public class ThrowAttackState<T> : BaseCreatureAttackState<T> where T : Creature
{
	private string resourceLocation = "Prefabs/Weapons/Throwing/Sword";
	private float throwingSpeed = 12f;
	private Transform locationHolder;
	private const string TEMP_LOC_NAME = "<TempLocHold>";

	protected override void StateStart(StateInfo info)
	{
		if (effected.PlatformerMovement2D.OnGround)
		{
			effected.PlatformerMovement2D.StopMoveVelocity();
		}

		effected.AnimationManager.PlayAnimation("Throw");

		effected.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		effected.AnimationManager.AnimationEndedEvent += OnAnimationEndedEvent;

		StateResourceLocationInfo resourceInfo = info.Get<StateResourceLocationInfo>();
		StateSpeedInfo speedInfo = info.Get<StateSpeedInfo>();
		StateLocationInfo locationInfo = info.Get<StateLocationInfo>();

        if (resourceInfo != null)
		{
			resourceLocation = resourceInfo.ResourceLocation;
        }

		if (speedInfo != null)
		{
			throwingSpeed = speedInfo.Speed;
		}

		if(locationInfo != null)
		{
			if (locationHolder == null)
			{
				locationHolder = new GameObject(TEMP_LOC_NAME).transform;
				locationHolder.SetParent(effected.transform, false);
				locationHolder.transform.position = locationInfo.Location;
			}
        }


		if (locationHolder == null)
		{
			locationHolder = effected.transform;
		}
	}

	protected override void StateEnd()
	{
		base.StateEnd();
        effected.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
	}

	private void OnAnimationEndedEvent(string animationName, float timeEndedNormalized)
	{
		if(effected.AnimationManager.CurrentAnimationNameCheck("Throw"))
		{
			ThrowWeapon();
			stateMachine.SwitchToDefaultState();
		}
	}

	private void ThrowWeapon()
	{
		ThrowingWeapon weapon = GameObject.Instantiate(Resources.Load<ThrowingWeapon>(resourceLocation));
		weapon.transform.position = locationHolder.position;
        weapon.Throw(throwingSpeed, new Vector2(Mathf.Abs(effected.transform.localScale.x) / effected.transform.localScale.x, 0), effected);

		if (locationHolder.name == TEMP_LOC_NAME)
		{
			GameObject.Destroy(locationHolder.gameObject);
		}
		locationHolder = null;
	}

	protected override void StateUpdate()
	{

	}
}
