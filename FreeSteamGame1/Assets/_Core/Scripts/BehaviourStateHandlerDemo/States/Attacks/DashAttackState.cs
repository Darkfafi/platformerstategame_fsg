﻿using Ramses.StateMachine;
using UnityEngine;

public class DashAttackState : BaseCreatureAttackState<PlayerCharacter>
{
	private float duration = 0.6f;
	private float timeDurationPassed = 0;

	protected override void StateStart(StateInfo info)
	{
        effected.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		effected.AnimationManager.AnimationEndedEvent += OnAnimationEndedEvent;

		if (!effected.AnimationManager.CurrentAnimationNameCheck("SkillMid"))
		{
			timeDurationPassed = 0;
			effected.AnimationManager.PlayAnimation("SkillStart");
		}
	}

	protected override void StateUpdate()
	{
		if(effected.AnimationManager.CurrentAnimationNameCheck("SkillMid"))
		{
			timeDurationPassed += Time.deltaTime;
            effected.PlatformerMovement2D.Move((int)effected.transform.localScale.x, 1.3f);

			if (timeDurationPassed >= duration)
			{
				stateMachine.SwitchToDefaultState();
			}
		}
	}

	protected override void StateEnd()
	{
		base.StateEnd();
		effected.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		effected.PlatformerMovement2D.Move((int)effected.transform.localScale.x, 0.65f);
	}

	private void OnAnimationEndedEvent(string animation, float finishNormTime)
	{
		if (effected.AnimationManager.CurrentAnimationNameCheck("SkillStart"))
		{
			effected.AnimationManager.PlayAnimation("SkillMid");
		}
	}

}
