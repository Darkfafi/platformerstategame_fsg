﻿using System;
using UnityEngine;
using Ramses.StateMachine;

public class PlayerCombatSwitcher : StateSwitcher<PlayerCharacter>
{
	private PlayerCharacter player;

	protected override void SwitcherActivate()
	{
		this.player = effected;

		player.InputUser.InputEvent -= OnInputEvent;
		player.InputUser.InputEvent += OnInputEvent;
	}

	protected override void SwitcherUpdate()
	{
		
	}

	protected override void SwitcherDeactivate()
	{
		player.InputUser.InputEvent -= OnInputEvent;
	}

	private void OnInputEvent(InputAction action, InputUser user)
	{
		if (action.KeyActionValue == InputAction.KeyAction.OnKeyDown)
		{
			if (action.Name == InputNames.BASIC_ATTACK)
			{
				stateMachine.SwitchToState<MeleeAttackState<PlayerCharacter>>();
			}

			if(action.Name == InputNames.RANGED_ATTACK)
			{
				stateMachine.SwitchToState<ThrowAttackState<PlayerCharacter>>(
					new StateLocationInfo(new Vector2(
						(player.transform.position.x + (0.45f * Mathf.Abs(player.transform.localScale.x) / player.transform.localScale.x)),
						(player.transform.position.y + 0.3f)
					)
				));
			}

			if(action.Name == InputNames.SPECIAL_ATTACK)
			{
				stateMachine.SwitchToState<DashAttackState>();
			}
		}
	}
}
