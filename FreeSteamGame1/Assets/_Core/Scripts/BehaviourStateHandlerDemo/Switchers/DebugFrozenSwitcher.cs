﻿using UnityEngine;
using System.Collections;
using Ramses.StateMachine;
using System;

public class DebugFrozenSwitcher<T> : StateSwitcher<T> where T : Creature, IFreazable
{
	protected override void SwitcherActivate()
	{

	}

	protected override void SwitcherDeactivate()
	{

	}

	protected override void SwitcherUpdate()
	{
		// DEBUG FROZEN STATE
		if (Input.GetKeyDown(KeyCode.Space))
		{
			effected.Freeze(new StateInfo());
		}
	}
}
