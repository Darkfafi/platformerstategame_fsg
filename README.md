# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a quick demo on the [BehaviourStateSystem](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Utils(v2)/BehaviourStateSystem/?at=master)

The BehaviourStateSystem is a system which can be used to creature character behaviour for both the Player and an AI.


It's build upon the idea that a 'BehaviourStateUser' Creates and updates a 'BehaviourStateHandler' and gives it its brain type.


This brain is where, for a player, the input is translated to a state and where, for an AI, a situation is translated into a state.


The BehaviourStateHandler that activates a state on command of outside sources and the brain itself. This state is free to do whatever it pleases with the BehaviourStateUser.


I would love to get some feedback on how to do some things better.

![Capture.PNG](https://bitbucket.org/repo/5b9L5y/images/2146731339-Capture.PNG)

### How do I get set up? ###

* Summary of set up
The system is controlled by the following items:
	// The System (Links are examples or the class itself depending on if they are inherited or used)
	- [IBehaviourStateUser](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Creatures/Creature.cs?at=master&fileviewer=file-view-default) : The one to be effected by the states. [Player](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Creatures/Player/PlayerCharacter.cs?at=master&fileviewer=file-view-default) : Inheriting the example and is the final set up for the handler 
	- [BehaviourStateHandler](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Utils(v2)/BehaviourStateSystem/BehaviourStateHandler.cs?at=master&fileviewer=file-view-default) 	: The effector. The one who will project the current state onto the user and which is to call to change states.
	- [StateSwitcher](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Creatures/States/Player/PlayerStateSwitcher.cs?at=master&fileviewer=file-view-default) 			: The brain which will control the manual states. The ones the User controlles himself and which desides his default state etc.
	- [BehaviourState](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Creatures/States/Player/PlayerMovementState.cs?at=master&fileviewer=file-view-default)			: A state which has the power to effect the user in any way it wants when activated.
	- [SelfEndingBehaviourState](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Creatures/States/FrozenState.cs?at=master&fileviewer=file-view-default)	: The same as a BehaviourState but this one has the power to end itself and return the user into its requested state.
	- [StateInfoPart](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Creatures/States/BehaviourStateInfo/StateDurationInfo.cs?at=master&fileviewer=file-view-default)				: The info which can be catched by states if they desire. The state is requested to have a default value for if the info is not found.
	- [BehaviourStateInfo](https://bitbucket.org/Darkfafi/platformerstategame_fsg/src/8fe7034639b0324dd535e94dc6e69b9222618e7c/FreeSteamGame1/Assets/_Core/Scripts/Utils(v2)/BehaviourStateSystem/BehaviourStateInfo/BehaviourStateInfo.cs?at=master&fileviewer=file-view-default)		: Simply a container for all the StateInfoParts given to a state by the setter.

### Contribution guidelines ###

- The IBehaviourStateUser must provide an Update Loop for the BehaviourStateHandler it created.
- A StateInfoPart must be a struct, else errors will occur!
- If a state is open for info parts it must ALWAYS have a default value for when the StateInfoPart has no value. It must also always check for a value with the 'HasValue' method.
- The SelfEndingBehaviourState will automaticly call the GetReturnState() in the StateSwitcher to get set its return state after it ended itself with the SelfEnd() method.

### Who do I talk to? ###

* Ramses Di Perna. The Netherlands.
* To contact me, please visit my website http://ramsesdiperna.com/ for contact methods. or add me on Skype: 'Darkfafi'