﻿using UnityEngine;
using Ramses.BehaviourStates;

public class FrozenState : SelfEndingBehaviourState<Creature>
{
	private Color originalColor;
	private float timeFrozen = 0f;
	private float duration;

	public override void OnStateStart(Creature user, BehaviourStateInfo info)
	{
		base.OnStateStart(user, info);
		originalColor = user.SpriteRenderer.color;
        user.AnimationManager.PauseCurrentAnimation();
		user.SpriteRenderer.color = Color.cyan;
		if(info.Get<StateDurationInfo>().HasValue)
		{
			duration = info.Get<StateDurationInfo>().Value.Duration;
		}
		else
		{
			duration = 2f;
        }
    }

	public override void OnStateUpdate()
	{
		base.OnStateUpdate();
		timeFrozen += Time.deltaTime;
		if(timeFrozen >= duration)
		{
			SelfEnd();
		}
    }

	public override void OnStateEnded()
	{
		base.OnStateEnded();
		timeFrozen = 0;
        user.SpriteRenderer.color = originalColor;
	}
}
