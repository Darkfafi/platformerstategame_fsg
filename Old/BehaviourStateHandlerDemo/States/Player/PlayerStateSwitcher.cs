﻿using System;
using UnityEngine;
using Ramses.BehaviourStates;

public class PlayerStateSwitcher : StateSwitcher<PlayerCharacter>
{
	private PlayerCharacter player;

	protected override void AwakeStateSwitching(PlayerCharacter user)
	{
		this.player = user;
		player.BehaviourStateHandler.SetState<PlayerMovementState>();
		

		player.InputUser.InputEvent -= OnInputEvent;
		player.InputUser.InputEvent += OnInputEvent;
	}

	protected override void GlobalStateSwitching(PlayerCharacter user)
	{
		// DEBUG FROZEN STATE
		if (Input.GetKeyDown(KeyCode.Space))
		{
			if (!user.BehaviourStateHandler.IsInState<FrozenState>())
			{
				user.BehaviourStateHandler.SetState<FrozenState>();
			}
		}
	}

	public override Type GetReturnState(Type behaviourStateType)
	{
		if (!player.BehaviourStateHandler.IsInState(behaviourStateType))
		{
			if (behaviourStateType == typeof(FrozenState))
			{
				return player.BehaviourStateHandler.GetCurrentStateType();
			}
		}
		return typeof(PlayerMovementState);
	}

	protected override void DestroyStateSwitching(PlayerCharacter user)
	{
		player.InputUser.InputEvent -= OnInputEvent;
	}

	private void OnInputEvent(InputAction action, InputUser user)
	{
		if (action.KeyActionValue == InputAction.KeyAction.OnKeyDown)
		{
			if (player.BehaviourStateHandler.IsInState<PlayerMovementState>())
			{
				if (action.Name == InputNames.BASIC_ATTACK)
				{
					player.BehaviourStateHandler.SetState<MeleeAttackState>();
				}

				if(action.Name == InputNames.RANGED_ATTACK)
				{
					player.BehaviourStateHandler.SetState<ThrowAttackState>(
						new StateLocationInfo(new Vector2(
							(player.transform.position.x + (0.45f * Mathf.Abs(player.transform.localScale.x) / player.transform.localScale.x)),
							(player.transform.position.y + 0.3f)
						)
					));
				}

				if(action.Name == InputNames.SPECIAL_ATTACK)
				{
					player.BehaviourStateHandler.SetState<DashAttackState>();
				}
			}
		}
	}
}
