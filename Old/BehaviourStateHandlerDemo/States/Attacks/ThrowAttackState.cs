﻿using Ramses.BehaviourStates;
using UnityEngine;

public class ThrowAttackState : BaseCreatureAttackState
{
	private string resourceLocation = "Prefabs/Weapons/Throwing/Sword";
	private float throwingSpeed = 12f;
	private Transform locationHolder;
	private const string TEMP_LOC_NAME = "<TempLocHold>";

	public override void OnStateStart(Creature user, BehaviourStateInfo info)
	{
		base.OnStateStart(user, info);

		if (user.PlatformerMovement2D.OnGround)
		{
			user.PlatformerMovement2D.StopMoveVelocity();
		}

		user.AnimationManager.PlayAnimation("Throw");

		user.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		user.AnimationManager.AnimationEndedEvent += OnAnimationEndedEvent;

		locationHolder = user.transform;

		StateResourceLocationInfo? resourceInfo = info.Get<StateResourceLocationInfo>();
		StateSpeedInfo? speedInfo = info.Get<StateSpeedInfo>();
		StateLocationInfo? locationInfo = info.Get<StateLocationInfo>();

        if (resourceInfo.HasValue)
		{
			resourceLocation = resourceInfo.Value.ResourceLocation;
        }

		if(speedInfo.HasValue)
		{
			throwingSpeed = speedInfo.Value.Speed;
		}

		if(locationInfo.HasValue)
		{
			locationHolder = new GameObject(TEMP_LOC_NAME).transform;
			locationHolder.SetParent(user.transform, false);
			locationHolder.transform.position = locationInfo.Value.Location;
        }
    }

	public override void OnStateEnded()
	{
		base.OnStateEnded();
		if(locationHolder.name == TEMP_LOC_NAME)
		{
			Object.Destroy(locationHolder.gameObject);
		}
		locationHolder = null;
        user.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
	}

	private void OnAnimationEndedEvent(string animationName, float timeEndedNormalized)
	{
		if(user.AnimationManager.CurrentAnimationNameCheck("Throw"))
		{
			ThrowWeapon();
			SelfEnd();
		}
	}

	private void ThrowWeapon()
	{
		ThrowingWeapon weapon = Object.Instantiate(Resources.Load<ThrowingWeapon>(resourceLocation));
		weapon.transform.position = locationHolder.position;
        weapon.Throw(throwingSpeed, new Vector2(Mathf.Abs(user.transform.localScale.x) / user.transform.localScale.x, 0), user);
	}
}
