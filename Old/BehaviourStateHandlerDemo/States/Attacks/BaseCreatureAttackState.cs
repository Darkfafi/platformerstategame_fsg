﻿using UnityEngine;
using System.Collections;
using Ramses.BehaviourStates;

public abstract class BaseCreatureAttackState : SelfEndingBehaviourState<Creature>
{
	protected float cooldownInSeconds = 0.1f;
	private float lastTimeUsed = 0;

	public override bool IsAbleToActivate(Creature user)
	{
		if (Time.time - lastTimeUsed > cooldownInSeconds)
		{
			return true;
		}
		return false;
	}

	public override void OnStateEnded()
	{
		base.OnStateEnded();
		lastTimeUsed = Time.time;
	}
}