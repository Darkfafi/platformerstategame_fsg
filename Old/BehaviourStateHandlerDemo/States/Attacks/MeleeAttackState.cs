﻿using UnityEngine;
using Ramses.BehaviourStates;

public class MeleeAttackState : BaseCreatureAttackState
{
	public override void OnStateStart(Creature user, BehaviourStateInfo info)
	{
		base.OnStateStart(user, info);

		user.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		user.AnimationManager.AnimationEndedEvent += OnAnimationEndedEvent;

		if(user.PlatformerMovement2D.OnGround)
		{
			user.PlatformerMovement2D.StopMoveVelocity();
		}

		user.AnimationManager.PlayAnimation("Attack");
	}

	public override void OnStateEnded()
	{
		base.OnStateEnded();
		user.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
	}

	private void OnAnimationEndedEvent(string animation, float finishNormTime)
	{
		if (user.AnimationManager.CurrentAnimationNameCheck("Attack"))
		{
			SelfEnd();
		}
	}
}
