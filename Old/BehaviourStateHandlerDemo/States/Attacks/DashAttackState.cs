﻿using Ramses.BehaviourStates;
using UnityEngine;

public class DashAttackState : BaseCreatureAttackState
{
	private float duration = 0.6f;
	private float timeDurationPassed = 0;

	public override void OnStateStart(Creature user, BehaviourStateInfo info)
	{
		base.OnStateStart(user, info);
        user.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		user.AnimationManager.AnimationEndedEvent += OnAnimationEndedEvent;

		if (!user.AnimationManager.CurrentAnimationNameCheck("SkillMid"))
		{
			timeDurationPassed = 0;
			user.AnimationManager.PlayAnimation("SkillStart");
		}
	}

	public override void OnStateUpdate()
	{
		base.OnStateUpdate();
		if(user.AnimationManager.CurrentAnimationNameCheck("SkillMid"))
		{
			timeDurationPassed += Time.deltaTime;
            user.PlatformerMovement2D.Move((int)user.transform.localScale.x, 1.3f);

			if (timeDurationPassed >= duration)
			{
				SelfEnd();
			}
		}
	}

	public override void OnStateEnded()
	{
		base.OnStateEnded();
        user.AnimationManager.AnimationEndedEvent -= OnAnimationEndedEvent;
		user.PlatformerMovement2D.Move((int)user.transform.localScale.x, 0.65f);
	}

	private void OnAnimationEndedEvent(string animation, float finishNormTime)
	{
		if (user.AnimationManager.CurrentAnimationNameCheck("SkillStart"))
		{
			user.AnimationManager.PlayAnimation("SkillMid");
		}
	}

}
