﻿using Ramses.BehaviourStates;

public struct StateResourceLocationInfo : IStateInfoPart
{
	public string ResourceLocation;

	public StateResourceLocationInfo(string resourceLocation)
	{
		ResourceLocation = resourceLocation;
	}
}
