﻿using Ramses.BehaviourStates;

public struct StateSpeedInfo : IStateInfoPart
{
	public float Speed;

	public StateSpeedInfo(float speed)
	{
		Speed = speed;
	}
}
