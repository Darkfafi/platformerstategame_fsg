﻿using UnityEngine;
using Ramses.BehaviourStates;

public struct StateLocationInfo :  IStateInfoPart
{
	public Vector2 Location;

	public StateLocationInfo(Vector2 location)
	{
		Location = location;
    }
}
