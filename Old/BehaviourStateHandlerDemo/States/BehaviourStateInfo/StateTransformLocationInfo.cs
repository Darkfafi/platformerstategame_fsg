﻿using UnityEngine;
using Ramses.BehaviourStates;

public struct StateTransformLocationInfo : IStateInfoPart
{
	public Transform Transform;

	public StateTransformLocationInfo(Transform transform)
	{
		Transform = transform;
    }
}
