﻿using Ramses.BehaviourStates;

public struct StateDurationInfo : IStateInfoPart
{
	public float Duration;

	public StateDurationInfo(float durationInSeconds)
	{
		Duration = durationInSeconds;
    }
}
