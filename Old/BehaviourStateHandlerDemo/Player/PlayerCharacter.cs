﻿using UnityEngine;
using Ramses.BehaviourStates;

[RequireComponent(typeof(InputUser))]
public class PlayerCharacter : Creature
{
	public InputUser InputUser { get; private set; }

	protected override void SetCreature()
	{
		base.SetCreature();
		InputUser = gameObject.GetComponent<InputUser>();
		((CreatureAnimationManager)AnimationManager).SetAnimationHandler(this);
		BehaviourStateHandler = new BehaviourStateHandler<PlayerStateSwitcher>(this);
	}
}
