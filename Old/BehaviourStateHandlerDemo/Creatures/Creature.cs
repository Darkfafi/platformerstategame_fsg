﻿using UnityEngine;
using Ramses.BehaviourStates;
using System;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D), typeof(AnimationManager))]
public class Creature : MassEntity, IBehaviourStateUser
{
	public IBehaviourStateHandler BehaviourStateHandler { get; protected set; }

	public BusyList BusyList { get; private set; }
	public MovementStats MovementStats { get { return _movementStats; } }
	public PlatformerMovement2D PlatformerMovement2D { get; private set; }
	public AnimationManager AnimationManager { get; private set; }
	public SpriteRenderer SpriteRenderer { get; private set; }

	protected Collider2D creatureCollider { get; private set; }
	protected Rigidbody2D creatureRigidBody2D { get; private set; }
	protected TouchDetector2D creatureTouchDetector2D { get; private set; }

	// Stats
	[SerializeField]
	private MovementStats _movementStats;

	protected override void Awake()
	{
		base.Awake();
		BusyList = new BusyList();
		SetCreature();
	}

	protected virtual void SetCreature()
	{
		AnimationManager =			gameObject.GetComponent<AnimationManager>();
		creatureCollider =			gameObject.GetComponent<Collider2D>();
		creatureRigidBody2D =		gameObject.GetComponent<Rigidbody2D>();
		creatureTouchDetector2D =	gameObject.AddComponent<TouchDetector2D>();
		SpriteRenderer =			gameObject.GetComponent<SpriteRenderer>();
        creatureTouchDetector2D.RunOn(creatureCollider);
		PlatformerMovement2D = new PlatformerMovement2D(transform, creatureCollider, creatureRigidBody2D, creatureTouchDetector2D);
	}

	protected void Update()
	{
		if(BehaviourStateHandler != null)
		{
			BehaviourStateHandler.UpdateState();
		}
	}

	protected override void OnDestroy()
	{
		base.OnDestroy();
		if (BehaviourStateHandler != null)
		{
			BehaviourStateHandler.CleanStateHandler();
		}
	}
}
