﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class ThrowingWeapon : MonoBehaviour
{
	public Creature Thrower { get; private set; }
	private Rigidbody2D rBody2D;

	public void Throw(float speed, Vector2 direction, Creature thrower)
	{
		Thrower = thrower;
		Vector3 v = Quaternion.LookRotation(direction, Vector3.up).eulerAngles;
		v.z = v.y - 90;
		v.y = 0;
		v.x = 0;
        transform.rotation = Quaternion.Euler(v);
		rBody2D.velocity = new Vector2(transform.right.x, transform.right.y) * speed + (Vector2.up * (speed / 6));
	}

	protected void Awake()
	{
		rBody2D = GetComponent<Rigidbody2D>();
	}

	protected void Update()
	{
		// DEBUG FROZEN STATE
		if(Thrower != null && rBody2D.velocity.magnitude < 0.2f)
		{
			Thrower.BehaviourStateHandler.SetState<FrozenState>();
			Destroy(this);
        }
	}
}
